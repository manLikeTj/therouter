import Home from './components/Home.vue'
import Account from './components/Account'
import AddEvent from './components/AddEvent'
import AddEvent2 from './components/AddEvent2'
import BuyMore from './components/BuyMore'
import EventPage from './components/EventPage'
import GiftCard from './components/GiftCard'
import Listing from './components/Listing'
import PastEvent from './components/PastEvent'
import PaymentOptions from './components/PaymentOptions'
import Settings from './components/Settings'
import Terms from './components/Terms'
import TicketDetailPage from './components/TicketDetailPage'
import Upcoming from './components/Upcoming'


/*function load(componentName) {
    return () =>
        import ('@components/${componentName}')
}
*/

export const routes = [{
        path: '/',
        component: Home
            //name: 'Home',
            //component: load('Home')
    },
    {
        path: '/event',
        component: EventPage
            //name: 'EventPage',
            //component: load('EventPage')
    },
    {
        path: '/account',
        component: Account
            //name: 'Account',
            //component: load('Account')
    },
    {
        path: '/add-event',
        component: AddEvent
            //name: 'AddEvent',
            //component: load('AddEvent')
    },
    { path: '/add-event-2', component: AddEvent2 },
    { path: '/buy-more', component: BuyMore },
    { path: '/gift-card', component: GiftCard },
    { path: '/listing', component: Listing },
    { path: '/past-event', component: PastEvent },
    { path: '/payment-options', component: PaymentOptions },
    { path: '/settings', component: Settings },
    { path: '/terms', component: Terms },
    { path: '/ticket-detail', component: TicketDetailPage },
    { path: '/upcoming-event', component: Upcoming },
]

export default routes;